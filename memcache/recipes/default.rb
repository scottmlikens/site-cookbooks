#
# Cookbook Name:: memcache
# Recipe:: default
#
# Copyright 2009, Scott M. Likens
#

%w{libevent-devel}.each do |p|
  package(p)
end

remote_file "/usr/src/memcached-1.4.1.tar.gz" do
  source "http://memcached.googlecode.com/files/memcached-1.4.1.tar.gz"
  owner "root"
  group "root"
  mode "0644"
  not_if { FileTest.exists?("/usr/src/memcached-1.4.1.tar.gz") }
end

execute "unarchive-memcached" do
  command "cd /usr/src;tar zxfv memcached-1.4.1.tar.gz"
  only_if { FileTest.exists?("/usr/src/memcached-1.4.1.tar.gz") }
  not_if { FileTest.directory?("/usr/src/memcached-1.4.1") }
end

execute "configure-memcached" do
  command "cd /usr/src/memcached-1.4.1;./configure --prefix=/usr --sysconfdir=/etc"
  not_if { FileTest.exists?("/usr/src/memcached-1.4.1/Makefile") }
  only_if { FileTest.directory?("/usr/src/memcached-1.4.1") }
end

execute "make-memcached" do
  command "cd /usr/src/memcached-1.4.1;make -j8"
  not_if { FileTest.exists?("/usr/src/memcached-1.4.1/memcached") }
  only_if { FileTest.exists?("/usr/src/memcached-1.4.1/Makefile") }
end

execute "install-memcached" do
  command "cd /usr/src/memcached-1.4.1;make install"
  not_if { FileTest.exists?("/usr/bin/memcached") }
  only_if { FileTest.exists?("/usr/src/memcached-1.4.1/memcached") }
end

remote_file "/etc/init.d/memcached" do
  source "memcached"
  mode 0755
  owner "root"
  group "root"
end

service "memcached" do
  action [:enable, :start]
end

