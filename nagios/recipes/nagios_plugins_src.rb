#
# Cookbook Name:: nagios
# Recipe:: default
#
# Copyright 2009, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

remote_file "/usr/src/nagios-plugins-1.4.13.tar.gz" do
  source "http://prdownloads.sourceforge.net/sourceforge/nagiosplug/nagios-plugins-1.4.13.tar.gz"
  checksum "592fe3329777e53f8fb526cb61e625a85ae9a1fd850b4039f78a2c460b1607c5"
  not_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13.tar.gz") }
end

execute "un-unarchive nagios-plugins" do
  command "cd /usr/src;tar zxfv nagios-plugins-1.4.13.tar.gz"
  only_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13.tar.gz") }
  not_if { FileTest.directory?("/usr/src/nagios-plugins-1.4.13") }
end

execute "configure nagios-plugins" do
  command "cd /usr/src/nagios-plugins-1.4.13;./configure --prefix=/usr/local/nagios --with-mysql"
  only_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13/configure") }
  not_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13/Makefile") }
end

execute "make nagios-plugins" do
  command "cd /usr/src/nagios-plugins-1.4.13;3.2.0;make all -j4"
  only_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13/Makefile") }
  not_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13/plugins/check_http") }
end

execute "install nagios-plugins" do
  command "cd /usr/src/nagios-plugins-1.4.13;make install"
  only_if { FileTest.exists?("/usr/src/nagios-plugins-1.4.13/plugins/check_http") }
  not_if { FileTest.exists?("/usr/local/nagios/libexec/check_http") }
end
