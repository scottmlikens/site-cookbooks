#
# Cookbook Name:: nagios
# Recipe:: default
#
# Copyright 2009, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

remote_file "/usr/src/nagios-3.2.0.tar.gz" do
  source "http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-3.2.0.tar.gz"
  checksum "7cbf35ba0319f24fa085982c038437c4515003b27863c4897dc86b197a50a5e7"
  not_if { FileTest.exists?("/usr/src/nagios-3.2.0.tar.gz") }
end

execute "un-unarchive nagios" do
  command "cd /usr/src;tar zxfv nagios-3.2.0.tar.gz"
  only_if { FileTest.exists?("/usr/src/nagios-3.2.0.tar.gz") }
  not_if { FileTest.directory?("/usr/src/nagios-3.2.0") }
end

group "nagios" do
  gid 320
end

user "nagios" do
  comment "nagios"
  gid "nagios"
  uid 320
  home "/usr/local/nagios"
  supports :manage_home => false
  shell "/sbin/false"
end

directory "/usr/local/nagios" do
  owner "nagios"
  group "nagios"
  mode "0750"
end

execute "configure nagios" do
  command "cd /usr/src/nagios-3.2.0;./configure --prefix=/usr/local/nagios --with-nagios-usernagios --with-nagios-group=nagios"
  not_if { FileTest.exists?("/usr/src/nagios-3.2.0/Makefile") }
end

execute "make nagios" do
  command "cd /usr/src/nagios-3.2.0;make all -j4"
  not_if { FileTest.exists?("/usr/src/nagios-3.2.0/base/nagios") }
end

execute "install nagios" do
  command "cd /usr/src/nagios-3.2.0;make install all"
  only_if { FileTest.exists?("/usr/src/nagios-3.2.0/base/nagios") }
  not_if { FileTest.exists?("/usr/local/nagios/bin/nagios") }
end

execute "install nagios-init.d" do
  command "cd /usr/src/nagios-3.2.0;make install-daemoninit"
  only_if { FileTest.exists?("/usr/local/nagios/bin/nagios") }
  not_if { FileTest.exists?("/etc/init.d/nagios") }
end


