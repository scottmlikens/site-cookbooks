#
# Cookbook Name:: nagios
# Recipe:: default
#
# Copyright 2009, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

remote_file "/usr/src/nrpe-2.12.tar.gz" do
  source "http://prdownloads.sourceforge.net/sourceforge/nagios/nrpe-2.12.tar.gz"
  not_if { FileTest.exists?("/usr/src/nrpe-2.12.tar.gz") }
end

execute "unarchive nrpe" do
  command "cd /usr/src;tar zxfv nrpe-2.12.tar.gz"
  not_if { FileTest.directory?("/usr/src/nrpe-2.12") }
  only_if { FileTest.exists?("/usr/src/nrpe-2.12.tar.gz") }
end

execute "configure nrpe" do
  command "cd /usr/src/nrpe-2.12;./configure --prefix=/usr/local/nagios"
  not_if { FileTest.exists?("/usr/src/nrpe-2.12/Makefile") }
  only_if { FileTest.exists?("/usr/src/nrpe-2.12/configure") }
end

execute "make nrpe" do
  command "cd /usr/src/nrpe-2.12;make all"
  only_if { FileTest.exists?("/usr/src/nrpe-2.12/Makefile") }
  not_if { FileTest.exists?("/usr/src/nrpe-2.12/src/nrpe") }
end

execute "install nrpe" do
  command "cd /usr/src/nrpe-2.12;make install"
  only_if { FileTest.exists?("/usr/src/nrpe-2.12/src/nrpe") }
  not_if { FileTest.exists?("/usr/local/nagios/bin/nrpe") }
end

execute "install /etc/init.d/nrpe" do
  command "cd /usr/src/nrpe-2.12;cp init-script /etc/init.d/nrpe"
  not_if { FileTest.exists?("/etc/init.d/nrpe") }
end

