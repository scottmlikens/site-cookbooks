#
# Cookbook Name:: snmp
# Recipe:: default
#
# Copyright 2009, Adapp, Inc.

packages = case node[:platform]
  when "centos","redhat","fedora"
    %w{net-snmp}
  else
    %w{snmpd}
  end
  
packages.each do |pkg|
  package pkg
end

remote_file "/etc/default/snmpd" do
  source "snmpd"
  mode 0644
  owner "root"
  group "root"
  only_if do platform?("ubuntu", "debian") end
end

service "snmpd" do
  action [:enable, :start]
end

remote_file "/etc/snmp/snmpd.conf" do
  source "snmpd.conf"
  mode 0644
  owner "root"
  group "root"
  notifies :restart, resources(:service => "snmpd")
end

